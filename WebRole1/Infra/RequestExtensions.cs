﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Security;
using WebRole1.Models;

namespace WebRole1.Infra
{
	public static class RequestExtensions
	{
		public static AuthSession GetCurrentSession(this HttpRequestBase request)
		{
			var cooki = request.Cookies[FormsAuthentication.FormsCookieName];
			if (cooki == null || String.IsNullOrEmpty(cooki.Value))
				return null;

			FormsAuthenticationTicket authTicket;
			try
			{
				authTicket = FormsAuthentication.Decrypt(cooki.Value);
			}
			catch
			{
				return null;
			}

			var sid = Guid.Parse(authTicket.Name);

			AuthSession authSession = null;
			using (var db = new DatabaseEntities())
			{
				authSession = db.AuthSessions.Where(a => a.SessionId == sid).Include("User").SingleOrDefault();
			}

			return authSession;
		}
	}
}