﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace WebRole1.Infra
{
	public static class Extensions
	{
		

		public static string ConvertUrlsToLinks(this string input)
		{
			var output = input;

			var regx = new Regex("([^\"])(http(s)?://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*([a-zA-Z0-9\\?\\#\\=\\/]){1})?)", RegexOptions.IgnoreCase);

			MatchCollection mactches = regx.Matches(output);

			foreach (Match match in mactches)
			{
				var displayUrl = match.Groups[2].Value.Replace("http://", "");
				if (displayUrl.Length > 40)
					displayUrl = displayUrl.Substring(0, 40).Trim(new char[] { ' ', '.', ',', '/' }) + "...";

				output = output.Replace(match.Value, match.Groups[1].Value + "<a href=\"" + match.Value + "\" target=\"_blank\">" + displayUrl + "</a>");
			}
			return output;
		}

		public static string HighlightStuff(this string input)
		{
			input = Regex.Replace(input, "@([^ ]+?)\\b", "<span class=\"mention\"><a href=\"https://twitter.com/$1\" target=\"_blank\">@$1</a></span>", RegexOptions.IgnoreCase);
			input = Regex.Replace(input, "(^|\\s|\\.)#(.+?)\\b", "$1<span class=\"hashtag\"><a href=\"https://twitter.com/search/%23$2\" target=\"_blank\">#$2</a></span>", RegexOptions.IgnoreCase);

			return input;
		}

		public static string CompressString(this string input)
		{
			return input.Replace("\n", "").Replace("\t", "").Replace("  ", " ").Replace("\r", "");
		}

		public static string ToAbbreviatedFormat(this TimeSpan span)
		{
			if (span.Days > 0)
				return String.Format("{0} d ", span.Days);

			if (span.Hours > 0)
				return String.Format("{0} h ", span.Hours);

			if (span.Minutes > 0)
				return String.Format("{0} m ", span.Minutes);

			return span.Seconds + " s";
		}
	}
}