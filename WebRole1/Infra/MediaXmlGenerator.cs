﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace WebRole1.Infra
{
	/// <summary>
	/// Processes Tweet's text & extra information to extract all entities and generate mediaXml for their previews.
	/// </summary>
	public class MediaXmlGenerator
	{
		public static Regex instagramRegex = new Regex(@"http://instagr.am/p/([a-zA-Z0-9_-]+)/?", RegexOptions.Singleline);
		public static Regex yfrogRegex = new Regex(@"http://yfrog.com/([a-zA-Z0-9_-]+)", RegexOptions.Singleline);
		public static Regex twitPicRegex = new Regex(@"http://twitpic.com/([a-zA-Z0-9_-]+)", RegexOptions.Singleline);

		public static string Generate(string text, XElement mediaXml)
		{
			var collection = new MediaCollection
				{
					Images = new List<MediaImage>()
				};

			ProcessEmbeddedMedia(mediaXml, collection);
			ProcessInstagram(text, collection);
			ProcessYFrog(text, collection);
			ProcessTwitpic(text, collection);

			if (collection.Images.Count == 0)
				return null;

			return collection.ToString();
		}

		private static void ProcessInstagram(string text, MediaCollection collection)
		{
			foreach (Match match in instagramRegex.Matches(text))
			{
				string shortUrl = match.Value;
				string code = match.Groups[1].Value;
				string preview = "http://instagr.am/p/" + code + "/media/?size=l";
				string fullUrl = shortUrl;

				collection.Images.Add(new MediaImage
					{
						FullUrl = fullUrl,
						Preview = preview,
						ShortUrl = shortUrl
					});
			}
		}

		private static void ProcessYFrog(string text, MediaCollection collection)
		{
			foreach (Match match in yfrogRegex.Matches(text))
			{
				string shortUrl = match.Value;
				string code = match.Groups[1].Value;
				string preview = shortUrl + ":iphone";
				string fullUrl = shortUrl;

				collection.Images.Add(new MediaImage
					{
						FullUrl = fullUrl,
						Preview = preview,
						ShortUrl = shortUrl
					});
			}
		}

		private static void ProcessTwitpic(string text, MediaCollection collection)
		{
			foreach (Match match in twitPicRegex.Matches(text))
			{
				string shortUrl = match.Value;
				string code = match.Groups[1].Value;
				string preview = "http://twitpic.com/show/large/" + code + ".jpg";
				string fullUrl = shortUrl;

				collection.Images.Add(new MediaImage
					{
						FullUrl = fullUrl,
						Preview = preview,
						ShortUrl = shortUrl
					});
			}
		}

		private static void ProcessEmbeddedMedia(XElement mediaXml, MediaCollection collection)
		{
			// Check media 
			if (mediaXml == null || mediaXml.IsEmpty)
				return;

			foreach (XElement creative in mediaXml.Elements("creative"))
			{
				string url = creative.Element("url").Value;
				string mediaUrl = creative.Element("media_url").Value;
				string expandedUrl = creative.Element("expanded_url").Value;

				collection.Images.Add(new MediaImage
					{
						Preview = mediaUrl,
						ShortUrl = url,
						FullUrl = expandedUrl
					});
			}
		}
	}
}