﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WebRole1.Infra
{
	public static class Settings
	{
		public static string ConsumerKey
		{
			get
			{
				return ConfigurationManager.AppSettings["twitterConsumerKey"];
			}
		}

		public static string ConsumerSecret
		{
			get
			{
				return ConfigurationManager.AppSettings["twitterConsumerSecret"];
			}
		}
	}
}