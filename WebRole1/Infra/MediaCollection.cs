﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace WebRole1.Infra
{
	public class MediaImage
	{
		/// <summary>
		/// ShortUrl as it appears in tweet's text
		/// </summary>
		public string ShortUrl { get; set; }

		/// <summary>
		/// URL of the image that should be used as a preview
		/// </summary>
		public string Preview { get; set; }

		/// <summary>
		/// Full URL where user should be redirected if he needs more info
		/// </summary>
		public string FullUrl { get; set; }
	}

	public class MediaCollection
	{
		public List<MediaImage> Images { get; set; } 

		public static MediaCollection Parse(string xml)
		{
			var xdoc = XDocument.Parse(xml);
			var images = xdoc.Root.Elements("image").Select(image => new MediaImage
				{
					Preview = image.Element("Preview").Value, 
					ShortUrl = image.Element("ShortUrl").Value, 
					FullUrl = image.Element("FullUrl").Value,
				}).ToList();

			return new MediaCollection() {Images = images};
		}

		public override string ToString()
		{
			var xdoc = new XDocument();
			var media = new XElement("media");
			xdoc.Add(media);

			if (Images != null)
			{
				foreach (var image in Images)
				{
					var mediaImg = new XElement("image",
					                            new XElement("Preview", image.Preview),
					                            new XElement("ShortUrl", image.ShortUrl),
					                            new XElement("FullUrl", image.FullUrl));

					media.Add(mediaImg);
				}
			}

			return xdoc.ToString();
		}
	}
}