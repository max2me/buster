﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Hammock;
using Hammock.Authentication.OAuth;
using Hammock.Web;
using WebRole1.Models;

namespace WebRole1.Infra
{
	public class TwitterApi
	{
		private User _User;

		public TwitterApi(User user)
		{
			_User = user;
		}

		private RestClient GetClient(WebMethod method = WebMethod.Get)
		{
			var credentials = new OAuthCredentials
			{
				Type = OAuthType.ProtectedResource,
				SignatureMethod = OAuthSignatureMethod.HmacSha1,
				ParameterHandling = OAuthParameterHandling.HttpAuthorizationHeader,
				ConsumerKey = Settings.ConsumerKey,
				ConsumerSecret = Settings.ConsumerSecret,
				Token = _User.Token,
				TokenSecret = _User.Secret
			};

			var client = new RestClient
			{
				Authority = "https://api.twitter.com",
				Credentials = credentials,
				Method = method
			};

			return client;
		}

		#region Receive New Tweets

		public void ReceiveNewTweets(int count = 200, long sinceTweetID = 0)
		{
			var xml = GetTimelineXml(count, sinceTweetID);

			SaveResponseReceived(xml, "timeline." + DateTime.Now.Ticks + ".xml");

			var x = XDocument.Parse(xml);

			var statuses = (from s in x.Root.Elements("status") select GetStatus(s)).Where(s => s != null).ToList();

			using (var db = new DatabaseEntities())
			{
				db.Attach(_User);

				foreach (var status in statuses)
				{
					var c = db.Statuses.Count(s => s.OriginalTweetId == status.OriginalTweetId && s.User.UserId == _User.UserId);
					if (status.Text.Length > 800)
						status.Text = status.Text.Substring(0, 800);

					if (c == 0)
						_User.Statuses.Add(status);
				}

				db.SaveChanges();
			}
		}

		private void SaveResponseReceived(string xml, string filename)
		{
			#if DEBUG
			System.IO.File.WriteAllText(HttpContext.Current.Server.MapPath("~/App_Data/" + DateTime.Now.Ticks + "." + filename), xml);
			#endif
		}

		public string GetTimelineXml(int count, long sinceTweetID)
		{
			var client = GetClient();
			var request = new RestRequest
			{
				Path = "/1/statuses/home_timeline.xml"
			};


			request.AddParameter("count", count.ToString());
			request.AddParameter("include_rts", "1");
			request.AddParameter("include_entities", "1");

			if (sinceTweetID > 0)
				request.AddParameter("since_id", sinceTweetID.ToString());


			return client.Request(request).Content;
		}

		private Status GetStatus(XElement x)
		{
			if (x == null)
				return null;

			var xu = x.Element("user");
			if (xu == null)
				return null;

			var s = new Status()
			{
				OriginalTweetId = Int64.Parse(x.Element("id").Value),
				Text = x.Element("text").Value,
				Created = ParseDateTime(x.Element("created_at").Value),

				AuthorId = Int32.Parse(xu.Element("id").Value),
				AuthorName = xu.Element("name").Value,
				AuthorScreenName = xu.Element("screen_name").Value,
				AuthorAvatar =  xu.Element("profile_image_url").Value
			};

			// Saving in reply to for use in conversations
			var inreplytoXml = x.Element("in_reply_to_status_id");
			if (inreplytoXml != null && !String.IsNullOrWhiteSpace(inreplytoXml.Value) )
				s.ReplyToTweetId = long.Parse(inreplytoXml.Value);

			var urls = x.Element("entities").Element("urls");
			if (urls.HasElements)
			{
				foreach (var url in urls.Elements("url"))
				{
					s.Text = s.Text.Replace(url.Element("url").Value, url.Element("expanded_url").Value);
				}
			}

			// Has to abe after we expanded t.co urls
			s.MediaXml = MediaXmlGenerator.Generate(s.Text, x.Element("entities").Element("media"));

			// Use full text of the retweet
			// Otherwise only 140 characters are shown
			var xretweeted = x.Element("retweeted_status");
			if (xretweeted != null)
			{
				var rs = GetStatus(xretweeted);
				s.Text = "RT @" + rs.AuthorScreenName + ": " + rs.Text;
			}

			return s;
		}

		

		#endregion

		public string Favorite(long tweet)
		{
			var client = GetClient(WebMethod.Post);
			var request = new RestRequest
			{
				Path = "/1/favorites/create/" + tweet + ".xml"
			};

			return client.Request(request).Content;
		}

		public string Retweet(long tweet)
		{
			var client = GetClient(WebMethod.Post);
			var request = new RestRequest
			{
				Path = "/1/statuses/retweet/" + tweet + ".xml"
			};

			return client.Request(request).Content;
		}

		public string Reply(long replyTo, string status)
		{
			var client = GetClient(WebMethod.Post);
			var request = new RestRequest
			{
				Path = "/1/statuses/update.xml"
			};

			request.AddParameter("status", status);
			request.AddParameter("in_reply_to_status_id", replyTo.ToString());

			return client.Request(request).Content;
		}

		public static DateTime ParseDateTime(string date)
		{
			string dayOfWeek = date.Substring(0, 3).Trim();
			string month = date.Substring(4, 3).Trim();
			string dayInMonth = date.Substring(8, 2).Trim();
			string time = date.Substring(11, 9).Trim();
			string offset = date.Substring(20, 5).Trim();
			string year = date.Substring(25, 5).Trim();
			string dateTime = string.Format("{0}-{1}-{2} {3}", dayInMonth, month, year, time);
			DateTime ret = DateTime.Parse(dateTime);
			return ret;
		}

		public class TwitterUser
		{
			public string Name { get; set; }
			public string ScreenName { get; set; }
			public string Location { get; set; }
			public string Description{ get; set; }
			public bool Following { get; set; }
			public int FollowersCount { get; set; }
		}

		public TwitterUser ShowUser(string screenname)
		{
			var client = GetClient(WebMethod.Get);
			var request = new RestRequest
			{
				Path = "/1/users/show.xml"
			};

			request.AddParameter("screen_name", screenname);

			var resp = client.Request(request).Content;
			
			SaveResponseReceived(resp, "user." + screenname + ".xml");

			return ParseUser(resp);
		}

		public TwitterUser Follow(string screenname)
		{
			return ChangeFriendship(screenname, true);
		}

		public TwitterUser UnFollow(string screenname)
		{
			return ChangeFriendship(screenname, false);
		}

		private TwitterUser ChangeFriendship(string screenname, bool follow)
		{
			var client = GetClient(WebMethod.Post);
			var request = new RestRequest
				{
					Path = follow ? "/1/friendships/create.xml" : "/1/friendships/destroy.xml"
				};

			request.AddParameter("screen_name", screenname);

			var resp = client.Request(request).Content;

			SaveResponseReceived(resp, "user." + (follow ? "" : "un") + "follow." + screenname + ".xml");

			return ParseUser(resp);
		}


		private static TwitterUser ParseUser(string resp)
		{
			var x = XDocument.Parse(resp);

			var following = x.Root.Element("following").Value;
			var f = !String.IsNullOrWhiteSpace(following) && bool.Parse(following);

			var user = new TwitterUser()
			{
				Name = x.Root.Element("name").Value,
				ScreenName = x.Root.Element("screen_name").Value,
				Location = x.Root.Element("location").Value,
				Description = x.Root.Element("description").Value,
				Following = f, // Have to use IsNullOrWhiteSpace because sometimes Twitter returns empty string
				FollowersCount = Int32.Parse(x.Root.Element("followers_count").Value),
			};
			return user;
		}

	}
}