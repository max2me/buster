﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using WebRole1.Models;
using WebRole1.ViewModels.Timeline;

namespace WebRole1.Infra
{
	public class TimelineGenerator
	{
		public TimelineGenerator(User user)
		{
			User = user;
		}

		private User User { get; set; }

		public TimelineFeed GetData()
		{
			var result = new TimelineFeed();

			using (var db = new DatabaseEntities())
			{
				db.Attach(User);

				IEnumerable<Status> statuses = GetTweetsForDisplay(db);

				Packet[] packets = (from status in statuses
				                    group status by status.AuthorId
				                    into packetCandidate
				                    select GetPacket(db, packetCandidate)
				                   ).OrderByDescending(p => p.Author.Pinned).ThenByDescending(p => p.MostRecentTweet.Created).ToArray();


				var formattedIDs = new StringBuilder();
				foreach (Packet packet in packets)
				{
					foreach (Tweet t in packet.Tweets)
					{
						formattedIDs.AppendFormat("{0};", t.ID);
					}
				}

				var sessionID = Guid.NewGuid();
				var session = new TimelineSession
					{
						TimelineSessionId = sessionID,
						User = User,
						TweetIds = formattedIDs.ToString()
					};

				db.TimelineSessions.AddObject(session);
				db.SaveChanges();

				result.SessionID = sessionID;
				result.Package = packets;
			}

			foreach (var packet in result.Package)
			{
				ProcessPacket(packet);
			}

			return result;
		}

		private void ProcessPacket(Packet packet)
		{
			const int threshold = 12; // When number of tweets exceeds threshold, we apply collapsing
			const int tweetsVisible = 4; // That's how many tweets will be visible together with group

			packet.Portions = new List<IPortion>();

			if (packet.Tweets.Length > threshold)
			{
				var group = new TweetGroup()
					{
						Title = (packet.Tweets.Length - tweetsVisible) + " earlier tweets"
					};

				// Fill the group
				for (var i = 0; i < packet.Tweets.Length - tweetsVisible; i++)
					group.Tweets.Add(packet.Tweets[i]);

				packet.Portions.Add(group);

				// Add the rest
				for (var i = packet.Tweets.Length - tweetsVisible; i < packet.Tweets.Length; i++)
					packet.Portions.Add(packet.Tweets[i]);
			}
			else
			{
				foreach (var t in packet.Tweets)
					packet.Portions.Add(t);	
			}
		}

		private IEnumerable<Status> GetTweetsForDisplay(DatabaseEntities db)
		{
			DateTime utcTime = DateTime.Now.ToUniversalTime();

			return (from status in db.Statuses
			        let authorState = (from a in db.AuthorStates where a.UserId == User.UserId && a.AuthorId == status.AuthorId select a).FirstOrDefault()
			        where (status.UserId == User.UserId &&
			               EntityFunctions.DiffHours(status.Created, utcTime) < 24 && // Tweet is not older than 24 hours
			               (authorState == null || authorState.LastReadStatusId < status.OriginalTweetId))
			        // It either first recognizable tweet from this owner or it hasnt been read
			        group status by status.OriginalTweetId
			        into grouped
			        select grouped.FirstOrDefault());
		}

		private Packet GetPacket(DatabaseEntities db, IGrouping<int, Status> packetCandidate)
		{
			Status status = packetCandidate.First();
			AuthorState u = db.AuthorStates.FirstOrDefault(os => os.UserId == User.UserId && os.AuthorId == status.AuthorId);
			bool isPinned = u != null && u.IsPinned;

			return new Packet
				{
					Author = new Author
						{
							ID = status.AuthorId,
							Name = status.AuthorName,
							ScreenName = status.AuthorScreenName,
							Avatar = status.AuthorAvatar,
							Pinned = isPinned,
						},
					Tweets = (from userStatuses in packetCandidate
					          select GetTweet(userStatuses)).OrderBy(us => us.Created).ToArray()
				};
		}

		private Tweet GetTweet(Status userStatus)
		{
			string text = ProcessMedia(" " + userStatus.Text, userStatus.MediaXml).ConvertUrlsToLinks().HighlightStuff();

			return new Tweet
				{
					Created = userStatus.Created,
					ID = userStatus.OriginalTweetId,
					Text = text,
					CreatedFormatted = (DateTime.Now.ToUniversalTime() - userStatus.Created).ToAbbreviatedFormat()
				};
		}

		private static string ProcessMedia(string text, string media)
		{
			if (String.IsNullOrWhiteSpace(media))
				return text;

			string output = text;

			MediaCollection collection = MediaCollection.Parse(media);
			foreach (MediaImage image in collection.Images)
			{
				string displayUrl = image.ShortUrl.Replace("http://", "").Replace("https://", "");
				output = output.Replace(image.ShortUrl, String.Format(@"<a href=""{1}"" data-preview=""{2}"" class=""media"">{0}</a>", displayUrl, image.FullUrl, image.Preview));
			}

			return output;
		}
	}
}