﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace WebRole1.App_Start
{
	public class BundleConfig
	{
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new StyleBundle("~/Content/homepage").Include("~/Content/base.css", "~/Content/homepage.css"));

			bundles.Add(new StyleBundle("~/Content/timeline").Include("~/Content/base.css", "~/Content/timeline.css", "~/Content/timeline.tweets.css"));
			bundles.Add(new ScriptBundle("~/Scripts/timeline").Include(
				"~/Scripts/timeline.plugins.js",
				"~/Scripts/timeline.actions.js",
				"~/Scripts/timeline.actions.mention.js",
				"~/Scripts/timeline.load.js"));
		}
	}
}