﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebRole1.Models;

namespace WebRole1.Controllers
{
    public class ScheduledController : Controller
    {
        //
        // GET: /Scheduled/

        public ActionResult Index()
        {
            return View();
        }

		public ActionResult SyncActiveUsers()
		{
			StringBuilder sb = new StringBuilder();

			using (var db = new DatabaseEntities())
			{
				var activeUsers = (from user in db.Users where user.ScreenName == "max2me" select user).Take(50);
				foreach (var user in activeUsers)
				{
					try
					{
						db.Detach(user);
						TimelineController.SyncTweets(user);
						sb.AppendFormat("Successfully synced @{0}<br/>", user.ScreenName);
					}
					catch (Exception)
					{
						sb.AppendFormat("Error occured while syncing for @{0}<br/>", user.ScreenName);
						throw;
					}
				}
			}


			ViewBag.Message = sb.ToString();
			return View("SyncResult");
		}

		public ActionResult ListAllUsers()
		{
			StringBuilder sb = new StringBuilder();

			using (var db = new DatabaseEntities())
			{
				var activeUsers = (from user in db.Users select user);
				foreach (var user in activeUsers)
				{
					try
					{
						var tweets = (from t in db.Statuses where t.UserId == user.UserId select t).Count();
						sb.AppendFormat("@{0} | {1}tweets<br/>", user.ScreenName, tweets);
					}
					catch (Exception)
					{
						sb.AppendFormat("Error occured while syncing for @{0}<br/>", user.ScreenName);
						throw;
					}
				}
			}


			ViewBag.Message = sb.ToString();
			return View("SyncResult");
		}
    }
}
