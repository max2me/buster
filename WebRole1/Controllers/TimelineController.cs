﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using WebRole1.Infra;
using WebRole1.Models;

namespace WebRole1.Controllers
{
	public class TimelineController : Controller
	{
		#region CurrentUser property

		private User _CurrentUser;
		public User CurrentUser
		{
			get
			{
				if (_CurrentUser == null)
				{
					var session = Request.GetCurrentSession();
					if (session != null)
						_CurrentUser = session.User;
				}

				return _CurrentUser;
			}
		}

		#endregion

		[Authorize]
		public ActionResult Index()
		{
			return View();
		}

		[Authorize]
		public ActionResult Sync()
		{
			SyncTweets(CurrentUser);

			ViewBag.Message = "Sync Completed";
			return View("Message");
		}

		[Authorize]
		public JsonResult GetTweets()
		{
			try
			{
				SyncTweets(CurrentUser);

				var timeline = new TimelineGenerator(CurrentUser);
				var feed = timeline.GetData();
				var response = new TweetsResponse
				               	{
				               		Html = RenderRazorViewToString("Feed", feed).CompressString()
				               	};

				return Json(response, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				var response = new TweetsResponse
				               	{
				               		IsError = true,
				               		Error = ex.ToString()
				               	};

				return Json(response, JsonRequestBehavior.AllowGet);
			}
		}
        
		[Authorize]
		public JsonResult MarkAsRead(Guid sessionId, long tweet)
		{
			using (var db = new DatabaseEntities())
			{
				var session = db.TimelineSessions.SingleOrDefault(s => s.TimelineSessionId == sessionId);
				if (session == null)
					throw new ApplicationException("Couldnt find appropriate session");

				var tweetIDs = session.TweetIds.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries).Select(long.Parse).ToList();
				var tweetIndex = tweetIDs.IndexOf(tweet);

				if (tweetIndex == -1)
					return Json(null);

				for (var i = 0; i <= tweetIndex; i++)
				{
					var id = tweetIDs[i];
					MarkTweetAsRead(db, CurrentUser, id);
				}

				var updatedTweetIDs = new StringBuilder();
				for (var i = tweetIndex + 1; i < tweetIDs.Count; i++)
				{
					updatedTweetIDs.AppendFormat("{0};", tweetIDs[i]);
				}

				session.TweetIds = updatedTweetIDs.ToString();
				db.SaveChanges();
			}

			return Json(null);
		}

		[Authorize]
		public JsonResult PinUser(int user, bool pin)
		{
			using (var db = new DatabaseEntities())
			{
				var authorState = db.AuthorStates.SingleOrDefault(os => os.UserId == CurrentUser.UserId && os.AuthorId == user);

				if (authorState != null)
				{
					authorState.IsPinned = pin;
					db.SaveChanges();
				}
				else
				{
					db.Attach(CurrentUser);

					authorState = new AuthorState()
						{
							LastReadStatusId = 0,
							User = CurrentUser,
							AuthorId = user,
							IsPinned = pin
						};

					db.AuthorStates.AddObject(authorState);
					db.SaveChanges();
				}
			}

			return Json(null);
		}

		[Authorize]
		public JsonResult Favorite(long tweet)
		{
			new TwitterApi(CurrentUser).Favorite(tweet);

			return Json(null);
		}

		[Authorize]
		public JsonResult Retweet(long tweet)
		{
			new TwitterApi(CurrentUser).Retweet(tweet);

			return Json(null);
		}

		[Authorize]
		public JsonResult Reply(long replyTo, string status)
		{
			new TwitterApi(CurrentUser).Reply(replyTo, status);

			return Json(null);
		}

		[Authorize]
		public ActionResult UserDetails(string screenname)
		{
			var api = new TwitterApi(CurrentUser);
			var user = api.ShowUser(screenname);
			user.Description = user.Description.ConvertUrlsToLinks().HighlightStuff();

			return View("UserDetails", user);
		}

		[Authorize]
		public ActionResult Follow(string screenname)
		{
			var api = new TwitterApi(CurrentUser);
			var user = api.Follow(screenname);
			user.Description = user.Description.ConvertUrlsToLinks().HighlightStuff();
			user.Following = true;

			return View("UserDetails", user);
		}

		[Authorize]
		public ActionResult UnFollow(string screenname)
		{
			var api = new TwitterApi(CurrentUser);
			var user = api.UnFollow(screenname);
			user.Description = user.Description.ConvertUrlsToLinks().HighlightStuff();
			user.Following = false;

			return View("UserDetails", user);
		}

		private static void MarkTweetAsRead(DatabaseEntities db, User user, long tweetId)
		{
			// Find who tweeted this one
			var tweet = db.Statuses.FirstOrDefault(s => s.UserId == user.UserId && s.OriginalTweetId == tweetId);
			if (tweet == null)
				throw new Exception("Huh... Requested tweet is not in db, wth!?");

			// Do we have state for this tweet's author already?
			var record = db.AuthorStates.FirstOrDefault(os => os.UserId == user.UserId && os.AuthorId == tweet.AuthorId);
			if (record != null)
			{
				if (record.LastReadStatusId > tweetId)
					return;

				record.LastReadStatusId = tweetId;
				db.SaveChanges();
				return;
			}
			
			// Nope, no state, let's create a new one
			db.Attach(user);

			record = new AuthorState()
				{
					LastReadStatusId = tweetId,
					User = user,
					AuthorId = tweet.AuthorId
				};

			db.AuthorStates.AddObject(record);
			db.SaveChanges();
		}

		public static void SyncTweets(User user)
		{
			// Figure out if we have synced before
			long mostRecentTweetId = 0L;
			using (var db = new DatabaseEntities())
			{
				var mostRecentTweet = (from status in db.Statuses where status.UserId == user.UserId orderby status.OriginalTweetId descending select status).FirstOrDefault();

				if (mostRecentTweet != null)
					mostRecentTweetId = mostRecentTweet.OriginalTweetId;
			}

			// Load new tweets
			var api = new TwitterApi(user);

			if (mostRecentTweetId == 0)
				api.ReceiveNewTweets();
			else
				api.ReceiveNewTweets(sinceTweetID: mostRecentTweetId);
		}

		public string RenderRazorViewToString(string viewName, object model)
		{
			ViewData.Model = model;
			using (var sw = new StringWriter())
			{
				var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
				var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
				viewResult.View.Render(viewContext, sw);
				viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
				return sw.GetStringBuilder().ToString();
			}
		}


		#region Nested type: TweetsResponse

		public class TweetsResponse
		{
			public string Error { get; set; }
			public string Html { get; set; }
			public bool IsError { get; set; }
		}

		#endregion
	}
}