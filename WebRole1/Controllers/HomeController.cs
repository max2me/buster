﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebRole1.Infra;

namespace WebRole1.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
	        var user = Request.GetCurrentSession();
			if (user != null)
				return Redirect("/Timeline");


            return View();
        }
    }
}
