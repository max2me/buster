﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Hammock;
using Hammock.Authentication.OAuth;
using WebRole1.Infra;
using WebRole1.Models;

namespace WebRole1.Controllers
{
	public class SignInController : Controller
	{
		//
		// GET: /SignIn/
		public ActionResult Index()
		{
			ServicePointManager.Expect100Continue = false;

			var credentials = new OAuthCredentials
			{
				Type = OAuthType.RequestToken,
				SignatureMethod = OAuthSignatureMethod.HmacSha1,
				ParameterHandling = OAuthParameterHandling.HttpAuthorizationHeader,
				ConsumerKey = Settings.ConsumerKey,
				ConsumerSecret = Settings.ConsumerSecret,
				CallbackUrl = "http://" + (Request.IsLocal ? "127.0.0.1" + ":" + Request.Url.Port : Request.Url.Host) + "/SignIn/TwitterCallback",
			};

			var client = new RestClient
			{
				Authority = "https://twitter.com/oauth",
				Credentials = credentials
			};

			var request = new RestRequest
			{
				Path = "/request_token"
			};

			RestResponse response = client.Request(request);
//
//			try
//			{
				var collection = HttpUtility.ParseQueryString(response.Content);
				Session["requestSecret"] = collection[1];

				return Redirect("https://twitter.com/oauth/authorize?oauth_token=" + collection[0]);
//			}
//			catch (Exception ex)
//			{
//				return Json(new { qs = response }, JsonRequestBehavior.AllowGet);
//			}
		}

		public ActionResult TwitterCallBack(string oauth_token, string oauth_verifier)
		{
			var requestSecret = (String)Session["requestSecret"];

			if (String.IsNullOrEmpty(oauth_token) || String.IsNullOrEmpty(requestSecret))
			{
				ViewBag.Message = "Token or verifier have not been provided.";
				return Redirect("/?denied");
			}

			// Grab data from Twitter
			var credentials = new OAuthCredentials
			{
				Type = OAuthType.AccessToken,
				SignatureMethod = OAuthSignatureMethod.HmacSha1,
				ParameterHandling = OAuthParameterHandling.HttpAuthorizationHeader,
				ConsumerKey = Settings.ConsumerKey,
				ConsumerSecret = Settings.ConsumerSecret,
				Token = oauth_token,
				TokenSecret = requestSecret,
				Verifier = oauth_verifier
			};

			var client = new RestClient()
			{
				Authority = "https://twitter.com/oauth",
				Credentials = credentials
			};

			var response = client.Request(new RestRequest
			{
				Path = "/access_token"
			});

			var responseCollection = HttpUtility.ParseQueryString(response.Content);

			ViewBag.Message = "";

			var accessToken = responseCollection["oauth_token"];
			var accessSecret = responseCollection["oauth_token_secret"];
			var userID = Int32.Parse(responseCollection["user_id"]);
			var screenName = responseCollection["screen_name"];

			AuthSession authSession;

			// Save session & user to db
			using (var db = new DatabaseEntities())
			{

				var user = db.Users.SingleOrDefault(u => u.UserId == userID);
				if (user == null)
				{
					user = new User()
					       	{
					       		ScreenName = screenName,
					       		UserId = userID,
								Secret = accessSecret,
								Token = accessToken
					       	};

					db.Users.AddObject(user);
				}
				else
				{
					user.Secret = accessSecret;
					user.Token = accessToken;
				}


				 authSession = new AuthSession()
				                  	{
				                  		SessionId = Guid.NewGuid(),
				                  		User = user,
				                  		Created = DateTime.Now
				                  	};

				db.AuthSessions.AddObject(authSession);
				db.SaveChanges();
			} 

			// Remember this user
//			Response.Cookies.Add(new HttpCookie("sid", authSession.SessionId.ToString()) { Expires = DateTime.Now.AddYears(5) });
			FormsAuthentication.SetAuthCookie(authSession.SessionId.ToString(), true);

			return RedirectToAction("Index", "Timeline");
		}

		public ActionResult SignOut()
		{
			FormsAuthentication.SignOut();
			return Redirect("~/");
		}
	}
}
