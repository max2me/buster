﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebRole1.ViewModels.Timeline
{
	/// <summary>
	/// Tweet author.
	/// </summary>
	public class Author
	{
		public string Avatar { get; set; }
		public int ID { get; set; }
		public string Name { get; set; }
		public bool Pinned { get; set; }
		public string ScreenName { get; set; }
	}
}