﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebRole1.ViewModels.Timeline
{
	public class TweetGroup : IPortion
	{
		public TweetGroup()
		{
			Tweets = new List<Tweet>();
		}

		public List<Tweet> Tweets { get; set; }
		public string Title { get; set; }

		public PortionType PortionType 
		{ 
			get
			{
				return PortionType.Group;
			}
		}
	}
}