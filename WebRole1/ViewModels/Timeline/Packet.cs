﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebRole1.Infra;

namespace WebRole1.ViewModels.Timeline
{
	/// <summary>
	/// Tweets from a single person.
	/// </summary>
	public class Packet
	{
		public Author Author { get; set; }

		public Tweet MostRecentTweet
		{
			get { return Tweets.OrderByDescending(t => t.Created).First(); }
		}

		public Tweet[] Tweets { get; set; }

		public List<IPortion> Portions { get; set; }
	}
}