﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebRole1.ViewModels.Timeline
{
	/// <summary>
	/// Overall object containing feed to be displayed.
	/// </summary>
	public class TimelineFeed
	{
		public Packet[] Package { get; set; }
		public Guid SessionID { get; set; }
	}
}