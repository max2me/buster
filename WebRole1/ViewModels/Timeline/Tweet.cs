﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebRole1.ViewModels.Timeline
{
	/// <summary>
	/// Tweet itself.
	/// </summary>
	public class Tweet : IPortion
	{
		public DateTime Created { get; set; }
		public string CreatedFormatted { get; set; }
		public long ID { get; set; }
		public int RetweetCount { get; set; }
		public string Text { get; set; }

		public PortionType PortionType 
		{ 
			get
			{
				return PortionType.Tweet;
			}
		}
	}
}