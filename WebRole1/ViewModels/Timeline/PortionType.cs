﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebRole1.ViewModels.Timeline
{
	public enum PortionType
	{
		Tweet, // Single tweet
		Group  // Multiple tweets collapsed
	}
}