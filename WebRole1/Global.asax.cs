﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using WebRole1.App_Start;

namespace WebRole1
{
	// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
	// visit http://go.microsoft.com/?LinkId=9394801
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
		}

//		private void Application_AuthenticateRequest(object sender, EventArgs eventArgs)
//		{
//			HttpCookie authCookie = Context.Request.Cookies[FormsAuthentication.FormsCookieName];
//			if (authCookie == null || authCookie.Value == "")
//				return;
//
//			FormsAuthenticationTicket authTicket;
//			try
//			{
//				authTicket = FormsAuthentication.Decrypt(authCookie.Value);
//			}
//			catch
//			{
//				return;
//			}
//
//			string[] roles = new string[] {"user"};
//
//			if (Context.User != null)
//				Context.User = new GenericPrincipal(Context.User.Identity, roles);
//		}
	}
}