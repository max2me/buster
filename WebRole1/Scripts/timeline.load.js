﻿var mostRecentTweet = null;
var mostRecentMarked = null;
var saveReadStateTimeout = null;

$(function () {
	$('#loading-error-showDetails').click(function (e) {
		e.preventDefault();
		$('#loading-error-details').toggle();
	});

	getNewTweets();
});

function getNewTweets() {
	$.ajax({
		type: 'GET',
		url: '/Timeline/GetTweets/?rand=' + Math.random(),
		dataType: 'json',
		data: {},

		success: function (data, textStatus, jqXHR) {
			if (data.IsError)
			{
				getNewTweets_showError(data.Error);
				return;
			}

			$('body').addClass('loaded');
			$('#tweets').html(data.Html);

			setupScroll();
			saveReadStateTimeout = setInterval(submitReadTweets, 3000);

			setupActions();
		},

		error: function (jqXHR, textStatus, errorThrown) {
			getNewTweets_showError(errorThrown);
		}
	});
}

function getNewTweets_showError(err) {
	$('body').addClass('loading-error');
	$('#loading-error-details').html(err.replace( /\n/ , '<br/>'));
}

function setupScroll() {
	$(window).scroll(function () {
		var tweets = $('#tweets .tweets .unread').get();

		var scrollTop = $(window).scrollTop();
		var viewportHeight = $(window).height();

		if (scrollTop + viewportHeight >= $(document).height() - 50) 
		{
			for (var i = 0; i < tweets.length; i++) 
			{
				var t = $(tweets[i]);
				t.addClass('read').removeClass('unread');
				
				var tweetID = t.attr('data-tweetid');
				mostRecentTweet = tweetID;
			}
		}
		else 
		{
			var threshold = screen.width > 640 ? 10 : 10; // Smaller value for phones

			for (var i = 0; i < tweets.length; i++) 
			{
				var t = $(tweets[i]);
				if (t.height() == 0)
					continue;
				
				if (t.offset().top + t.height() - 20 < scrollTop + threshold) // Entire package is out of the view
				{
					t.addClass('read').removeClass('unread');

					var tweetID = t.attr('data-tweetid');
					mostRecentTweet = tweetID;
				}
				else 
				{
					break; // All others are also unread, dont bother
				}
			}
		}
	});
}

function submitReadTweets() {
	if (mostRecentTweet == mostRecentMarked || mostRecentTweet == null)
		return;

	mostRecentMarked = mostRecentTweet;

	$.ajax({
		type: 'POST',
		url: '/Timeline/MarkAsRead',
		data: {
			'sessionId': $('#actions').data('sessionid'),
			'tweet': mostRecentTweet
		},

		success: function (data) {
		},

		error: function (jqXHR, textStatus, errorThrown) {
		}
	});
}