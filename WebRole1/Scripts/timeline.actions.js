﻿var currentTweet;
var currentUser;

function setupActions() {
	$('#tweets')
		.on('click', '.created', actions_Expand)
		.on('click', '.media', media)
		.on('click', '.mention a', mention)
		.on('click', '.package .user a, .package .screenname', mentionUser)
		.on('click', '.tweetgroup-title a', expandGroup);
	
	
	$('#actions-fav a').click(actions_Favorite);
	$('#actions-rt a').click(actions_Retweet);
	$('#actions-reply a').click(actions_Reply);
	$('.pin').click(actions_Pin);

	$('#replyForm form').submit(actions_ReplySubmit);
	$('#replyForm-cancel').click(actions_ReplyCancel);

	$('#markall a').click(actions_MarkAllAsRead);
}

function expandGroup(e) {
	e.preventDefault();
	e.stopPropagation();

	$(this)
		.parent()
		.hide()
		.parents('.tweetgroup')
		.find('.tweetgroup-tweets')
		.show();
}

function media(e) {
	e.preventDefault();
	e.stopPropagation();

	var a = $(this);
	var text = a.parents('.text');
	
	var img = a.data('preview');
	var fullUrl = a.attr('href');


	// Collapse img if current link has been expanded already
	var m = text.parent().find('.media-expanded a[href="' + fullUrl + '"]');
	if (m.length > 0)
	{
		m.parent().toggle();
		return;
	}
	
	// If this one is currently visible, remove it
	text
		.parent()
		.find('.media-expanded')
		.remove();

	var html = $('#media-expanded').html().replace('{expanded}', fullUrl);
	var expandedMedia = $(html)
		.hide()
		.appendTo(text.parent())
		.show();

	$('<img src="' + img + '"/>').load(function () {
		var loadedImage = $(this);
		loadedImage.hide();

		// Insert loaded image
		expandedMedia
			.find('a')
			.empty()
			.append(loadedImage)
			.end()
			.addClass('loaded');

		loadedImage.fadeIn(800);

		// Complicated math
		var scrollTop = $(window).scrollTop();
		var viewportHeight = $(window).height();
		var imgBottom = expandedMedia.offset().top + loadedImage.get(0).height;
		var imgHidden = imgBottom - scrollTop - viewportHeight;

		// If entire image will be visible
		if (imgHidden <= 0)
			return;

		$('html, body').animate({
			scrollTop: (scrollTop + imgHidden + 10) + 'px'
		}, 800);
	});
}

function actions_Reply(e) {
	e.preventDefault();
	
	var $actions = $('#actions').hide();
	$('#replyForm')
		.detach()
		.appendTo($actions.parent())
		.show()
		
		.find('textarea')
		.focus()
		.val(currentUser + ' ')
		.end()
		
		.find('input[name=replyTo]')
		.val(currentTweet);
}

function actions_ReplySubmit(e){
	e.preventDefault();

	var button = $('#replyForm input[type=submit]');
	var oldValue = button.val();
	button.val('Submitting...');

	var data = $(this).serializeFormJSON();
	$.ajax({
		type: 'POST',
		url: '/Timeline/Reply',
		data: data,

		success: function (data, textStatus, jqXHR) {
			$('#replyForm').hide();
			button.val(oldValue);
		},

		error: function (jqXHR, textStatus, errorThrown) {
			alert('We could not submit reply, please try again');
			button.val(oldValue);
		}
	});
}

function actions_ReplyCancel(e){
	$('#replyForm').hide();
	return false;
}

function actions_Retweet(e) {
	e.preventDefault();

	var tweet = $(this).parent();
	setState(tweet, 'wait');

	$.ajax({
		type: 'POST',
		url: '/Timeline/Retweet',
		data: {
			tweet: currentTweet,
			action: 'rt'
		},

		success: function (data, textStatus, jqXHR) {
			setState(tweet, 'done');
		},

		error: function (jqXHR, textStatus, errorThrown) {
			setState(tweet, 'error');
		}
	});
}

function actions_Expand (e) {
	e.preventDefault();
	e.stopPropagation();

	var a = $('#actions');

	a.find('li').each(function () {
		resetState($(this));
	});

	if (a.is(':visible') && $(this).parent().hasClass('actionsExpanded'))
	{
		a
			.hide()
			.parent()
			.removeClass('actionsExpanded');
		return;
	}

	var act = $('#actions').detach();
	var li = $(this).parent();

	currentTweet = li.attr('data-tweetid');
	currentUser = li.parents('.package').find('.user .screenname').text();

	li
		.find('.text')
		.after(act)
		.addClass('actionsExpanded');

	$('#actions').show();
}

function setState(element, state) {
	resetState(element);
	element.addClass(state);
}

function resetState(element) {
	element
		.removeClass('done')
		.removeClass('wait')
		.removeClass('error');
}

function actions_Favorite(e) {
	e.preventDefault();

	var li = $(this).parent();
	setState(li, 'wait');

	$.ajax({
		type: 'POST',
		url: '/Timeline/Favorite',
		data: {
			tweet: currentTweet,
			action: 'fav'
		},

		success: function (data, textStatus, jqXHR) {
			setState(li, 'done');
		},

		error: function (jqXHR, textStatus, errorThrown) {
			setState(li, 'error');
		}
	});
}

function actions_MarkAllAsRead (e) {
	var tweetID = $('#tweets .tweets .unread')
			.removeClass('unread')
			.addClass('read')
			.last()
			.attr('data-tweetid');

	mostRecentTweet = tweetID;
}

function actions_Pin(e) {
	e.preventDefault();

	var package = $(this).parents('.package');
	var isPinned = package.hasClass('pinned');

	$.post('/Timeline/PinUser',
				{
					user: $(this).parent().attr('data-userid'),
					pin: (!isPinned)
				});

	if (isPinned)
	{
		package.removeClass('pinned');
	}
	else
	{
		package.addClass('pinned');
	}
}