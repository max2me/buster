﻿function mention_close() {
	$(this)
		.parents('.userinfo')
		.slideUp(100);
}

function mention(e) {
	e.preventDefault();
	e.stopPropagation();

	var a = $(this);
	var name = a.text().replace('@', '');

	loadUserInfo(name, a.parents('.tweet').eq(0), true);
}

function mentionUser(e) {
	e.preventDefault();
	e.stopPropagation();

	var name = $(this).data('screenname');
	var container = $(this)
			.parents('.package')
			.find('.tweets .tweet')
			.eq(0);

	loadUserInfo(name, container, false);
}

function loadUserInfo(screenname, container, appendToTheEnd) {
	var ui;
	
	// Find existing user info panels for this user
	container
		.find('.userinfo')
		.each(function () {
			if ($(this).data('screenname') == screenname)
				ui = $(this);
		});

	// If found, remove it
	if (ui != null)
	{
		ui.slideUp(function () {
			ui.remove();
		});
		return;
	}

	// No existing mentions for this user, let's create a new one
	ui = $($('#userinfo').html());
	ui
		.hide()
		.data('screenname', screenname);

	// Append it
	if (appendToTheEnd)	
		ui.appendTo(container);
	else	
		ui.addClass('userinfo-added').prependTo(container);	
	
	ui.show();

	// Setup interaction events within
	$(ui)
		.on('click', '.ui-follow', follow)
		.on('click', '.ui-unfollow', follow)
		.on('click', '.close', mention_close);

	$.ajax({
		type: 'GET',
		url: '/Timeline/UserDetails',
		data: {
			screenname: screenname
		}
	})
		.done(function (data) {
			ui.html(data);
		})
		.fail(function () {
			ui.html('We could not load user info. Please try again in a few.');
		});
}

function follow(e) {
	e.preventDefault();
	e.stopPropagation();
	
	var state = $(this).parents('.state');
	setState(state, 'wait');

	var ui = $(this).parents('.userinfo');
	var isFollow = $(this).hasClass('ui-follow');

	$.ajax({
		type: 'POST',
		url: isFollow ? '/Timeline/Follow' : '/Timeline/Unfollow',
		data: {
			screenname: $(this).data('screenname')
		},

		success: function (data) {
			ui.html(data);
		},

		error: function () {
			setState(state, 'err');
		}
	});
}