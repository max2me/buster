﻿CREATE TABLE [dbo].[TimelineSessions]
(
	[TimelineSessionId] UniqueIdentifier NOT NULL PRIMARY KEY,
	[UserId] INT NOT NULL CONSTRAINT FK_TimelineSession2Users REFERENCES Users(UserId), 
	[TweetIds] ntext
)
