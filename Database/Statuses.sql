﻿CREATE TABLE [dbo].[Statuses]
(
	[StatusId] BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
	[OriginalTweetId] BIGINT NOT NULL , 
	[Created] DATETIME NOT NULL,
	[Text] NVARCHAR(1000) NOT NULL, 
	[AuthorId] INT NOT NULL, 
	[AuthorName] NVARCHAR(200) NOT NULL, 
	[AuthorScreenName] NVARCHAR(200) NOT NULL, 
	[AuthorAvatar] NVARCHAR(200) NOT NULL, 
	[UserId] int CONSTRAINT FK_Status2User REFERENCES Users(UserId) NOT NULL,
	[MediaXml] NTEXT NULL,
	[ReplyToTweetId] BIGINT NULL    
)
GO

CREATE INDEX [IX_OriginalTweetId]
	ON [dbo].[Statuses]
	(OriginalTweetId, UserId)
GO