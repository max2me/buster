﻿CREATE TABLE [dbo].[AuthorStates]
(
	[AuthorStateId] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[UserId] INT NOT NULL CONSTRAINT FK_AuthorStates2Users REFERENCES Users(UserId),
	[AuthorId] INT,
	[LastReadStatusId] BIGINT,
	[IsPinned] BIT NOT NULL DEFAULT 0
)
GO

CREATE INDEX [IX_AuthorStates_Select]
	ON [dbo].[AuthorStates]
	(AuthorId, UserId)
GO