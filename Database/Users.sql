﻿CREATE TABLE [dbo].[Users]
(
	[UserId] INT NOT NULL PRIMARY KEY, 
	[Token] NVARCHAR(50) NOT NULL, 
	[Secret] NVARCHAR(50) NOT NULL, 
    [ScreenName] NVARCHAR(50) NULL
)
